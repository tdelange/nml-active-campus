# NML Active Campus
This repository includes the code for the back-end and front-end. The APK (with user id = 1) and a manual for the participants when downloading from Google Drive.

## App Information
- Every 10 steps earns the user one point
- Currently the userId is hardcoded to id 1
- Locally steps get reset everday
- After every 100 steps are send to the server

## Server Infromation
- For deployment information check the readme inside the backend folder
- Everyday at 22:20:00 the daily steps get reset in the database and the daily steps are added on to the weekly steps
- Weekly steps get reset every week
- Everyday at 22:10:00 there are new general challenges inside the database



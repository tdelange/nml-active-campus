from datetime import datetime
from enum import IntEnum
from random import sample

from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_apscheduler import APScheduler
import os
import pandas as pd
from sqlalchemy import ForeignKey
from sklearn.cluster import KMeans


class ChallengeTypes(IntEnum):
    STEPS = 1
    TIME = 2
    LOCATION = 3


app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))

app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
    basedir, "db.sqlite"
)

# suppress SQLALCHEMY_TRACK_MODIFICATIONS warning
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# initialize scheduler
scheduler = APScheduler()


# --------------------------------------------------------------------------#
# DB Schema                                                                 #
# --------------------------------------------------------------------------#
db = SQLAlchemy(app)
ma = Marshmallow(app)


# Models
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    daily_steps = db.Column(db.Integer)
    weekly_steps = db.Column(db.Integer)

    def __init__(self, id, name, daily_steps, weekly_steps):
        self.id = id
        self.name = name
        self.daily_steps = daily_steps
        self.weekly_steps = weekly_steps


class Challenge(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(500))
    type = db.Column(db.Integer)
    points = db.Column(db.Integer)
    goal = db.Column(db.Integer)

    def __init__(self, id, name, type, points, goal):
        self.id = id
        self.name = name
        self.type = type
        self.points = points
        self.goal = goal


class History(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, ForeignKey("user.id"))
    date = db.Column(db.Date)
    total_points = db.Column(db.Integer)
    daily_steps = db.Column(db.Integer)
    total_completed_challenges = db.Column(db.Integer)

    def __init__(
        self, user_id, date, total_points, daily_steps, total_completed_challenges
    ):
        self.user_id = user_id
        self.date = date
        self.total_points = total_points
        self.daily_steps = daily_steps
        self.total_completed_challenges = total_completed_challenges


# --------------------------------------------------------------------------#
# Cluster on skill level of players                                         #
# --------------------------------------------------------------------------#
url = (
    "https://drive.google.com/file/d/1QYT5Q_022N0BzRCAR-IwMJZKTdb008Xs/view?usp=sharing"
)
file_id = url.split("/")[-2]
dwn_url = "https://drive.google.com/uc?id=" + file_id

history = pd.read_csv(dwn_url)
user_data = (
    history.groupby("user_id")
    .agg({"daily_steps": "mean", "total_completed_challenges": "mean"})
    .reset_index()
)
user_data["total_completed_challenges"] = round(
    user_data["total_completed_challenges"], 0
)

X = user_data[["daily_steps", "total_completed_challenges"]]

kmeans = KMeans(
    n_clusters=5, random_state=42
)  # Adjust the number of clusters as needed
kmeans.fit(X)

cluster_labels = kmeans.predict(X)

user_data["cluster"] = cluster_labels
average_steps = user_data.groupby("cluster")["daily_steps"].mean()
user_data = user_data.merge(
    average_steps.rename("avg_steps"), left_on="cluster", right_index=True
)
user_data["new_challenge"] = round(user_data["avg_steps"] + 500, -2)
user_data["new_challenge"] = [int(x) for x in user_data["new_challenge"]]


# --------------------------------------------------------------------------#
# Shedule Jobs                                                              #
# --------------------------------------------------------------------------#
# every day at 22:20:00 reset the daily steps and update the weekly
@scheduler.task(
    "cron",
    id="daily_step_reset",
    year="*",
    month="*",
    day="*",
    hour="22",
    minute="20",
    second="0",
)
def daily_step_reset():
    with app.app_context():
        all_users = User.query.all()
        for user in all_users:
            user.weekly_steps += user.daily_steps
            user.daily_steps = 0
        db.session.commit()


# every day at sunday reset the weekly steps
@scheduler.task("cron", id="weekly_steps_reset", week="*", day_of_week="sun")
def weekly_steps_reset():
    with app.app_context():
        all_users = User.query.all()
        for user in all_users:
            user.weekly_steps = 0
        db.session.commit()


challengesList = [
    Challenge(1, "Walk 1000 steps", ChallengeTypes.STEPS, 100, 1000),
    Challenge(2, "Walk 2000 steps", ChallengeTypes.STEPS, 200, 2000),
    Challenge(3, "Walk 3000 steps", ChallengeTypes.STEPS, 300, 3000),
    Challenge(4, "Walk 4000 steps", ChallengeTypes.STEPS, 400, 4000),
    Challenge(5, "Walk 5000 steps", ChallengeTypes.STEPS, 500, 5000),
    Challenge(6, "Walk 6000 steps", ChallengeTypes.STEPS, 600, 6000),
    Challenge(7, "Walk 7000 steps", ChallengeTypes.STEPS, 700, 7000),
    Challenge(8, "Walk 8000 steps", ChallengeTypes.STEPS, 800, 8000),
    Challenge(9, "Walk 9000 steps", ChallengeTypes.STEPS, 900, 9000),
]
# example of time challenge Challenge(4, "Walk for 20 minutes", ChallengeTypes.TIME, 200, 20)


# every day at 22:10:00 reset the challenges
@scheduler.task(
    "cron",
    id="daily_challenges_reset",
    year="*",
    month="*",
    day="*",
    hour="22",
    minute="20",
    second="0",
)
def daily_challenges_reset():
    with app.app_context():
        Challenge.query.delete()
        db.session.commit()
        activeChallenges = sample(challengesList, k=2)

        db.session.bulk_save_objects(activeChallenges)
        db.session.commit()


scheduler.init_app(app)
scheduler.start()


# schema
class UserSchema(ma.Schema):
    class Meta:
        fields = ("id", "name", "daily_steps", "weekly_steps")


class ChallengeSchema(ma.Schema):
    class Meta:
        fields = ("id", "name", "type", "points", "goal")


class HistorySchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "user_id",
            "date",
            "total_points",
            "daily_steps",
            "total_completed_challenges",
        )


# Initialize schema
user_schema = UserSchema()
users_schema = UserSchema(many=True)

challenge_schema = ChallengeSchema()
challenges_schema = ChallengeSchema(many=True)

historySchema = HistorySchema()
historiesSchema = HistorySchema(many=True)

with app.app_context():
    db.create_all()


# --------------------------------------------------------------------------#
# API Endpoints                                                             #
# --------------------------------------------------------------------------#


# Test endpoint
@app.route("/test", methods=["GET"])
def test():
    return "succesfully called test"


# Creates a history
@app.route("/history/<id>", methods=["POST"])
def add_history(id):
    user_id = id
    date = datetime.now().date()
    total_points = request.json["total_points"]
    daily_steps = request.json["daily_steps"]
    total_completed_challenges = request.json["total_completed_challenges"]

    new_history = History(
        user_id, date, total_points, daily_steps, total_completed_challenges
    )
    db.session.add(new_history)
    db.session.commit()

    return historySchema.jsonify(new_history)


# gets all histories
@app.route("/history", methods=["GET"])
def get_histories():
    histories = History.query.all()
    result = historiesSchema.dump(histories)
    return jsonify(result)


# Creates a new user
@app.route("/user", methods=["POST"])
def add_user():
    id = request.json["id"]
    name = request.json["name"]
    daily_steps = request.json["daily_steps"]
    weekly_steps = request.json["weekly_steps"]

    new_user = User(id, name, daily_steps, weekly_steps)
    db.session.add(new_user)
    db.session.commit()

    return user_schema.jsonify(new_user)


# Retrieves the specified users
@app.route("/user/<id>", methods=["GET"])
def get_user(id):
    user = User.query.get(id)
    result = user_schema.dump(user)
    return jsonify(result)


# Retrieves all the users
@app.route("/user", methods=["GET"])
def get_users():
    all_users = User.query.all()
    result = users_schema.dump(all_users)
    return jsonify(result)


# Update a user with new steps
@app.route("/user/<id>", methods=["PUT", "PATCH"])
def execute_todo(id):
    user = User.query.get(id)

    user.daily_steps = request.json["daily_steps"]
    db.session.commit()

    return user_schema.jsonify(user)


# Creates a new challenge
@app.route("/challenge", methods=["POST"])
def add_challenge():
    id = request.json["id"]
    name = request.json["name"]
    type = request.json["type"]
    points = request.json["points"]
    goal = request.json["goal"]

    new_challenge = Challenge(id, name, type, points, goal)
    db.session.add(new_challenge)
    db.session.commit()

    return challenge_schema.jsonify(new_challenge)


# Retrieves all the challenges
# It gives the user 3 general challenge and one individual
@app.route("/challenge/<id>", methods=["GET"])
def get_challenges(id):
    steps = int(user_data["new_challenge"][user_data["user_id"] == int(id)].iloc[0])

    all_challenges = Challenge.query.all()

    result = challenges_schema.dump(all_challenges)
    new_challenge = {
        "id": 999,
        "name": f"Walk {steps} steps",
        "type": ChallengeTypes.STEPS,
        "points": steps // 10,
        "goal": steps,
    }

    result.append(new_challenge)

    return result


# at the end for debugging
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)

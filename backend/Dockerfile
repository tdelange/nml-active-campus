# Interesting discussion that might favor multi-staged builds with venv
# https://stackoverflow.com/questions/53835198/integrating-python-poetry-with-docker

FROM python:3.9-slim-buster AS base

# --------------------------------------------- #
# Build stage                                   #
# --------------------------------------------- #
FROM base AS build

# Install pipenv and compilation dependencies
RUN python3 -m pip install pipenv

# Install python dependencies in /.venv
COPY Pipfile .
RUN pipenv lock
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy

# --------------------------------------------- #
# Runtime stage                                 #
# --------------------------------------------- #
FROM base AS runtime
WORKDIR /nml-backend

# Copy virtual env from build stage
COPY --from=build /.venv /.venv
ENV PATH="/.venv/bin:$PATH"

# Creating folders and files
COPY . .

ENV FLASK_APP=app/app.py
EXPOSE 8000

CMD ["flask", "run", "--host", "0.0.0.0", "--port", "8000"]
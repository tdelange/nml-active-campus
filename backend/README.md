# How to run on server
The hostname of the server is ```http://tsg-131-174-75-192.hosting.ru.nl``` and the backend port is ```8000```. So if you want to call the test endpoint for example, you can call ```http://tsg-131-174-75-192.hosting.ru.nl:8000/test```.

## Connecting to server
Connect to the server using the following command and entering the password. Note that you need to be on school wifi or on a school vpn to connect to the server. \
```ssh group5_1@131.174.75.192``` \
```hidde<3```

## Starting the app
If the app stopped, you can restart it using the following command: \
```docker start nml-backend```

## Updating the app
If you added new stuff and want it on the server, follow my lead. Firstly navigate to the project folder and pull the changes: \
```cd nml-active-campus``` \
```git pull origin main```

Now navigate to the backend folder and use the following command to rebuild the docker image: \
```docker build -t nml-backend backend/.```

Before you can run the newly built docker image, we first have to shut down, and delete the old the old container: \
```docker stop nml-backend``` \
```docker rm -f nml-backend```

Now run the new container using the following commandm the timezone is important for the cron jobs \
```docker run -e "TZ=Europe/Amsterdam" --name nml-backend -dp 8000:8000 nml-backend```

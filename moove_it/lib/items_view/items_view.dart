import 'package:flutter/material.dart';



class ItemButton extends StatelessWidget {
  static const double width = 120;
  static const double height = 80;
  static const double maxTextWidth = 100;

  final String itemName;
  final String? valueType;
  final int? displayValue;
  final Function() onPressed;

  const ItemButton({
    Key? key,
    required this.itemName,
    required this.onPressed,
    this.valueType,
    this.displayValue,
  }) : super(key: key);

  bool hasDisplayvalue() {
    return valueType != null && displayValue != null;
  }

  @override
  Widget build(BuildContext context) {
    final itemNameTextSpan = TextSpan(
      text: itemName,
      style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
    );

    final priceTextSpan = TextSpan(
      text: hasDisplayvalue() ? '$valueType: $displayValue' : '',
      style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
    );

    final itemNameTextPainter = TextPainter(
      text: itemNameTextSpan,
      textDirection: TextDirection.ltr,
    )..layout();

    final priceTextPainter = TextPainter(
      text: priceTextSpan,
      textDirection: TextDirection.ltr,
    )..layout();

    final itemNameWidth = itemNameTextPainter.width;
    final priceWidth = priceTextPainter.width;

    final hasTextOverflow =
        itemNameWidth > maxTextWidth || priceWidth > maxTextWidth;

    final padding =
        hasTextOverflow ? const EdgeInsets.all(8) : const EdgeInsets.all(16);

    return Center(
      child: SizedBox(
        width: width,
        height: height,
        child: ElevatedButton(
          onPressed: onPressed,
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.blue,
            foregroundColor: Colors.white,
            padding: padding,
            textStyle: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              RichText(
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                text: itemNameTextSpan,
              ),
              if (valueType != null && displayValue != null)
                RichText(
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  text: priceTextSpan,
                ),
            ],
          ),
        ),
      ),
    );
  }
}

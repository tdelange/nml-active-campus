import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/challenge.dart';

const url = 'http://tsg-131-174-75-192.hosting.ru.nl:8000/challenge';
// to call locally call: http://10.0.2.2:8000/challenge

Future<List<Challenge>> getChallenges(int id) async {
  final response = await http.get(
    Uri.parse('$url/$id'),
    headers: {'Content-Type': 'application/json'},
  );
  if (response.statusCode == 200) {
    final parsed = jsonDecode(response.body).cast<Map<String, dynamic>>();
    return parsed.map<Challenge>((json) => Challenge.fromJson(json)).toList();
  } else {
    throw Exception('Failed to get challenges');
  }
}

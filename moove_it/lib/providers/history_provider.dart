import 'dart:convert';

import 'package:http/http.dart' as http;

const url = 'http://tsg-131-174-75-192.hosting.ru.nl:8000/history';

Future<bool> postHistory(
    int userId, int points, int dailySteps, int completedChallenges) async {
  final response = await http.put(
    Uri.parse('$url/$userId'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, int>{
      'total_points': points,
      'daily_steps': dailySteps,
      'total_completed_challenges': completedChallenges
    }),
  );

  if (response.statusCode == 200) {
    return Future.value(true);
  } else {
    return Future.value(false);
  }
}

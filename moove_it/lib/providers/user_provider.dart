import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/user.dart';

const url = 'http://tsg-131-174-75-192.hosting.ru.nl:8000/user';
// to call locally call: http://10.0.2.2:8000/user

Future<List<User>> getUsers() async {
  final response = await http.get(
    Uri.parse(url),
    headers: {'Content-Type': 'application/json'},
  );
  if (response.statusCode == 200) {
    final parsed = jsonDecode(response.body).cast<Map<String, dynamic>>();
    return parsed.map<User>((json) => User.fromJson(json)).toList();
  } else {
    throw Exception('Failed to get users');
  }
}

Future<User> getUser() async {
  final response = await http.get(
    Uri.parse(url),
    headers: {'Content-Type': 'application/json'},
  );
  if (response.statusCode == 200) {
    return User.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to get users');
  }
}

Future<User> updateUserSteps(int dailySteps, int id) async {
  final response = await http.put(
    Uri.parse('$url/$id'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, int>{
      'daily_steps': dailySteps,
    }),
  );

  if (response.statusCode == 200) {
    return User.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to update steps');
  }
}

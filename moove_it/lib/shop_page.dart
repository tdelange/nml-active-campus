import 'package:flutter/material.dart';
import 'package:moove_it/state/items/inventory.dart';
import 'package:observer/observer.dart';

import 'items_view/category_header.dart';
import 'items_view/item_button.dart';
import 'state/items/item.dart';

class ShopPage extends StatefulWidget {
  static const int nColumns = 3;
  late final Inventory inventory;

  ShopPage(this.inventory, {super.key});

  @override
  State<StatefulWidget> createState() => _ShopPageState(inventory);
}

class _ShopPageState extends State<ShopPage> with Observer {
  late final Inventory inventory;
  _ShopPageState(this.inventory) {
    inventory.subscribe(this);
  }

  @override
  void update(Observable observable, Object itemUpdate) {
    if (mounted) {
      setState(() {
        // Re-renders widget if mounted (open)
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Shop'),
        ),
        body: ListView(
            children: inventory.unownedItems.entries
                .map<ShopCategoryGrid>((category) => ShopCategoryGrid(
                    nColumns: ShopPage.nColumns,
                    category: category.key,
                    items: category.value))
                .toList()));
  }
}

class ShopCategoryGrid extends StatelessWidget {
  static const double rowMargin = 1.5;
  final int nColumns;
  final String category;
  final List<Item> items;

  const ShopCategoryGrid(
      {super.key,
      required this.nColumns,
      required this.category,
      required this.items});

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      CategoryHeader(category: category),
      GridView.count(
          physics: const NeverScrollableScrollPhysics(),
          crossAxisCount: nColumns,
          shrinkWrap: true,
          childAspectRatio: rowMargin,
          children: [
            for (final item in items)
              ItemButton(
                  itemName: item.name,
                  subText: 'Price: ${item.price}',
                  onPressed: () => item.buy(context))
          ])
    ]);
  }
}

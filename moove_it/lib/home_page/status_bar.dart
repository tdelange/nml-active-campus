import 'package:flutter/material.dart';
import 'package:observer/observer.dart';

class StatusBar extends StatefulWidget {
  static const double height = 61.5;
  static const double width = 300;

  static const double iconSize = 36.0;
  static const double fontSize = 20.0;

  static const double barHeight = height;
  static const double barWidth = width - iconSize;
  static const double borderWidth = 2.0;

  static const double marginBetween = 10.0;

  final IconData icon;
  final double initialStatusFactor;
  final Function(Observer) subscribe;

  const StatusBar(
      {super.key,
      required this.icon,
      required this.initialStatusFactor,
      required this.subscribe});

  @override
  State<StatusBar> createState() => _StatusBarState(
        statusFactor: initialStatusFactor,
        subscribe: subscribe,
      );
}

class _StatusBarState extends State<StatusBar> with Observer {
  double statusFactor;

  _StatusBarState(
      {required this.statusFactor, required Function(Observer) subscribe}) {
    subscribe(this);
  }

  @override
  void update(Observable observable, Object newStatusFactor) {
    statusFactor = newStatusFactor as double;
    if (this.mounted) {
      setState(() {
        // Re-renders widget if mounted (open)
      });
    }
  }

  Color getColor() {
    int r = 255;
    int g = 255;
    int b = 0;
    double opacity = 1.0;

    if (statusFactor < 0.5) {
      g = (statusFactor * 255).toInt();
    }
    if (statusFactor > 0.5) {
      r = ((1 - statusFactor) * 2 * 255).toInt();
    }

    return Color.fromRGBO(r, g, b, opacity);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: StatusBar.height,
        width: StatusBar.width,
        child: Row(children: <Widget>[
          Icon(
            widget.icon,
            size: StatusBar.iconSize,
            color: Colors.grey,
          ),
          Expanded(
              child: Stack(
            alignment: Alignment.centerLeft,
            children: <Widget>[
              // Empty status bar
              Container(
                height: StatusBar.barHeight,
                width: StatusBar.barWidth,
                margin: const EdgeInsets.symmetric(
                    horizontal: StatusBar.marginBetween),
                decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.rectangle,
                    border: Border.all(width: StatusBar.borderWidth)),
              ),
              // Colouring the filled part of the status bar
              Container(
                height: StatusBar.barHeight,
                width: StatusBar.barWidth * statusFactor,
                margin: const EdgeInsets.symmetric(
                    horizontal: StatusBar.marginBetween),
                decoration: BoxDecoration(
                    color: getColor(),
                    shape: BoxShape.rectangle,
                    border: Border.all(width: StatusBar.borderWidth)),
              )
            ],
          ))
        ]));
  }
}

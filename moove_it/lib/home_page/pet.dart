import 'package:flutter/material.dart';
import 'package:moove_it/state/gamestate/state.dart';
import 'package:observer/observer.dart';
import 'package:widget_mask/widget_mask.dart';

class Pet extends StatefulWidget {
  final GameState _gameState;

  Pet(this._gameState, {super.key});

  @override
  State<StatefulWidget> createState() => _PetState(_gameState);
}

class _PetState extends State<Pet> with Observer {
  final GameState _gameState;

  _PetState(this._gameState) {
    _gameState.subscribePet(this);
    _gameState.subscribeColor(this);
    _gameState.subscribePattern(this);
  }

  @override
  void update(Observable observable, Object arg) {
    if (this.mounted) {
      setState(() {
        // Re-renders widget if mounted (open)
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 300,
      height: 300,
      child: WidgetMask(
        blendMode: BlendMode.srcOver,
        mask: Center(
          child: WidgetMask(
            blendMode: BlendMode.multiply,
            mask: Center(
              child: WidgetMask(
                blendMode: BlendMode.srcIn,
                mask: Container(
                    foregroundDecoration: BoxDecoration(
                        color: _gameState.color, backgroundBlendMode: BlendMode.multiply),
                    child: Image.asset(
                        'assets/images/patterns/${_gameState.pattern}.png')),
                child: Image.asset(
                    'assets/images/masks/${_gameState.pet}-mask.png'),
              ),
            ),
            child: Image.asset(
              'assets/images/pets/${_gameState.pet}.png',
              fit: BoxFit.cover,
            ),
          ),
        ),
        child: Image.asset('assets/images/background.png'),
      ),
    );
  }
}

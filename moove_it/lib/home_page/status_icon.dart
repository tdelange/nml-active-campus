import 'package:flutter/material.dart';
import 'package:observer/observer.dart';

class StatusIcon extends StatefulWidget {
  static const double height = 130.0;
  static const double width = 130.0;

  static const double iconSize = 100.0;
  static const double borderWidth = 2.0;
  static const double fontSize = 20.0;

  final IconData icon;

  final int initialStatus;
  final Function(Observer) subscribe;

  const StatusIcon(
      {super.key,
      required this.icon,
      required this.initialStatus,
      required this.subscribe});

  @override
  State<StatefulWidget> createState() =>
      _StatusIconState(status: initialStatus, subscribe: subscribe);
}

class _StatusIconState extends State<StatusIcon> with Observer {
  int status;

  _StatusIconState({required this.status, required Function(Observer) subscribe}) {
    subscribe(this);
  }

  @override
  void update(Observable observable, Object newStatus) {
    status = newStatus as int;
    if (this.mounted) {
      setState(() {
        // Re-renders widget if mounted (open)
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: StatusIcon.height,
        width: StatusIcon.width,
        decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
            border: Border.all(width: StatusIcon.borderWidth)),
        child: Center(
            child: Stack(alignment: Alignment.center, children: <Widget>[
          Icon(
            widget.icon,
            color: Colors.grey,
            size: StatusIcon.iconSize,
          ),
          Text(status.toString(),
              style: const TextStyle(
                  fontSize: StatusIcon.fontSize, color: Colors.black))
        ])));
  }
}

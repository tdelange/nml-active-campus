import 'package:flutter/material.dart';
import '../state/pedometer.dart';
import '../state/gamestate/state.dart';
import 'pet.dart';
import 'status_bar.dart';
import 'status_icon.dart';

class HomePage extends StatefulWidget {
  static const double margin = 22;
  static const double padding = 50;
  static const double goldenRatio = 1.618;

  final GameState gameState;
  final CustomPedometer pedometer;

  const HomePage(this.gameState, this.pedometer, {super.key});

  @override
  State<HomePage> createState() => _HomePageState(gameState, pedometer);
}

class _HomePageState extends State<HomePage> {
  final GameState gameState;
  final CustomPedometer pedometer;
  int todaySteps = 0;

  _HomePageState(this.gameState, this.pedometer) {
    todaySteps = pedometer.todaySteps;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(
            horizontal: HomePage.padding, vertical: HomePage.padding),
        child: Scaffold(
            body: Column(children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            StatusIcon(
                icon: Icons.directions_walk,
                initialStatus: 0,
                subscribe: pedometer.subscribeSteps),
            StatusIcon(
                icon: Icons.attach_money,
                initialStatus: gameState.points,
                subscribe: gameState.subscribePoints),
          ]),
          const SizedBox(height: HomePage.margin),
          Pet(gameState),
          const SizedBox(height: HomePage.margin),
          StatusBar(
              icon: Icons.emoji_emotions,
              initialStatusFactor: gameState.happiness,
              subscribe: gameState.subscribeHappiness),
          const SizedBox(height: HomePage.margin / HomePage.goldenRatio),
          StatusBar(
              icon: Icons.restaurant,
              initialStatusFactor: gameState.hunger,
              subscribe: gameState.subscribeHunger)
        ])));
  }
}

import 'package:flutter/material.dart';
import 'package:moove_it/models/user_leaderboard.dart';
import 'models/user.dart';
import 'providers/user_provider.dart';

class LeaderboardPage extends StatefulWidget {
  const LeaderboardPage({super.key});
  @override
  _LeaderboardPageState createState() => _LeaderboardPageState();
}

class _LeaderboardPageState extends State<LeaderboardPage> {
  late Future<List<User>> users;
  List<UserLeaderBoard> weeklyData = [];
  List<UserLeaderBoard> dailyData = [];

  @override
  void initState() {
    super.initState();
    users = getUsers();
  }

  bool _showWeekly = false;

  @override
  Widget build(BuildContext context) {
    const whiteTextStyle = TextStyle(
      color: Colors.white,
    );

    const boldWhiteTextStyle = TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.bold,
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text('Leaderboard'),
      ),
      body: SingleChildScrollView(
          child: FutureBuilder<List<User>>(
              future: users,
              builder: (context, snapshot) {
                if (snapshot.data == null) {
                  return const Text("Loading");
                } else {
                  var data = (snapshot.data as List<User>).toList();

                  List<UserLeaderBoard> weeklyData = data
                      .map((user) => UserLeaderBoard(
                          id: user.id,
                          name: user.name,
                          steps: user.weeklySteps))
                      .toList();

                  weeklyData.sort((a, b) => b.steps.compareTo(a.steps));

                  List<UserLeaderBoard> dailyData = data
                      .map((user) => UserLeaderBoard(
                          id: user.id, name: user.name, steps: user.dailySteps))
                      .toList();

                  dailyData.sort((a, b) => b.steps.compareTo(a.steps));

                  List<UserLeaderBoard> users =
                      _showWeekly ? weeklyData : dailyData;
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 16.0),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Row(
                          children: [
                            ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  _showWeekly = false;
                                });
                              },
                              style: ElevatedButton.styleFrom(
                                backgroundColor: _showWeekly
                                    ? Color.fromRGBO(188, 170, 164, 1)
                                    : null,
                              ),
                              child: const Text('Daily'),
                            ),
                            const SizedBox(width: 16.0),
                            ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  _showWeekly = true;
                                });
                              },
                              style: ElevatedButton.styleFrom(
                                backgroundColor: _showWeekly
                                    ? null
                                    : Color.fromRGBO(188, 170, 164, 1),
                              ),
                              child: const Text('Weekly'),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 16.0),
                      Center(
                        child: DataTableTheme(
                          data: DataTableThemeData(
                            headingRowColor: MaterialStateColor.resolveWith(
                              (states) => const Color.fromRGBO(141, 110, 99, 1),
                            ),
                            headingTextStyle: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 18.0,
                            ),
                          ),
                          child: DataTable(
                            sortAscending: false,
                            sortColumnIndex: 2,
                            columns: const <DataColumn>[
                              DataColumn(
                                label: Text(
                                  'Number',
                                  style: whiteTextStyle,
                                ),
                              ),
                              DataColumn(
                                label: Text(
                                  'Username',
                                  style: whiteTextStyle,
                                ),
                              ),
                              DataColumn(
                                label: Text(
                                  'Steps',
                                  style: whiteTextStyle,
                                ),
                              ),
                            ],
                            rows: users
                                .asMap()
                                .map((index, users) => MapEntry(
                                      index,
                                      DataRow(
                                        color: MaterialStateProperty.all(
                                            const Color.fromRGBO(
                                                188, 170, 164, 1)),
                                        cells: <DataCell>[
                                          DataCell(
                                            Text(
                                              (index + 1).toString(),
                                              style: whiteTextStyle,
                                            ),
                                          ),
                                          DataCell(
                                            Text(
                                              users.name,
                                              style: users.name == 'me'
                                                  ? boldWhiteTextStyle
                                                  : whiteTextStyle,
                                            ),
                                          ),
                                          DataCell(
                                            Text(
                                              users.steps.toString(),
                                              style: whiteTextStyle,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ))
                                .values
                                .toList(),
                          ),
                        ),
                      ),
                    ],
                  );
                }
              })),
    );
  }
}

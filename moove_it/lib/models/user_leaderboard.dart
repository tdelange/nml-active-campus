class UserLeaderBoard {
  final dynamic id;
  final String name;
  final int steps;

  const UserLeaderBoard({
    required this.id,
    required this.name,
    required this.steps,
  });

  factory UserLeaderBoard.fromJson(Map<String, dynamic> json) {
    return UserLeaderBoard(
      id: json['id'] as int,
      name: json['name'] as String,
      steps: json['steps'] as int,
    );
  }
}

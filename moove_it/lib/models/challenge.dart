import 'package:moove_it/state/gamestate/state.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Challenge {
  final dynamic challengeId;
  final String challengeDescription;
  final int points;
  final int goal;
  final int _type;
  late final String _persistanceName;
  int _progress = 0;

  Challenge({
    required this.challengeId,
    required this.challengeDescription,
    required this.points,
    required this.goal,
    required type,
  }) : _type = type {
    _persistanceName = 'challengeprogress@$challengeId';
    _init();
  }

  bool get completed => _progress >= goal;
  String get type {
    switch (_type) {
      case 0:
      default:
        return 'Steps';
    }
  }

  factory Challenge.fromJson(Map<String, dynamic> json) {
    var challengeId = json['id'] as int;
    // Challenge description / definition / title
    var challengeDescription = json['name'] as String;
    var type = json['type'] as int;

    var points = json['points'] as int;
    var goal = json['goal'] as int;

    return Challenge(
      challengeId: challengeId,
      challengeDescription: challengeDescription,
      points: points,
      goal: goal,
      type: type,
    );
  }

  complete(GameState gameState) {
    gameState.addPoints(points);
  }

  keepProgress(GameState gameState, int progress) {
    if (completed) return;

    _updateProgress(progress);

    if (completed) {
      complete(gameState);
    }
  }

  Future<void> _init() async {
    final prefs = await SharedPreferences.getInstance();
    final progress = prefs.getInt(_persistanceName);
    _progress = progress ?? 0;
  }

  Future<void> _updateProgress(int progress) async {
    if (progress != 0) {
      _progress = progress;
      final prefs = await SharedPreferences.getInstance();
      await prefs.setInt(_persistanceName, _progress);
    }
  }
}

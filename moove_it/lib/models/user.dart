class User {
  final dynamic id;
  final String name;
  final int dailySteps;
  final int weeklySteps;

  const User({
    required this.id,
    required this.name,
    required this.dailySteps,
    required this.weeklySteps,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'] as int,
      name: json['name'] as String,
      dailySteps: json['daily_steps'] as int,
      weeklySteps: json['weekly_steps'] as int,
    );
  }
}

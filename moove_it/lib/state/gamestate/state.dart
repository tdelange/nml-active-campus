import 'package:flutter/material.dart';
import 'package:observer/observer.dart';

import 'GameStateField.dart';

class GameState {
  static final GameState _instance = GameState._privateConstructor();
  static const Map<String, Object> startingValues = {
    'points': 100,
    'hunger': 0.3,
    'happiness': 0.5,
    'pet': 'cow',
    'color': Color.fromRGBO(255, 255, 255, 0.0),
    'pattern': 'no-pattern'
  };

  final GameStateFieldNumeric<int> _points = GameStateFieldInt("points");
  final GameStateFieldNumeric<double> _hunger = GameStateFieldDouble("hunger");
  final GameStateFieldNumeric<double> _happiness =
      GameStateFieldDouble("happiness");

  final CosmeticFieldColor _petColor = CosmeticFieldColor('color');
  final CosmeticFieldString _petPattern = CosmeticFieldString('pattern');
  final CosmeticFieldString _pet = CosmeticFieldString('pet');

  // hard coded user id
  final int userId = 1;

  GameState._privateConstructor();

  factory GameState() {
    return _instance;
  }

  get points => _points.value;
  get hunger => _hunger.value;
  get happiness => _happiness.value;

  get pet => _pet.value;
  get pattern => _petPattern.value;
  get color => _petColor.value;

  void subscribePoints(Observer observer) =>
      _points.observable.addObserver(observer);
  void subscribeHunger(Observer observer) =>
      _hunger.observable.addObserver(observer);
  void subscribeHappiness(Observer observer) =>
      _happiness.observable.addObserver(observer);
  void subscribePet(Observer observer) => _pet.observable.addObserver(observer);
  void subscribeColor(Observer observer) =>
      _petColor.observable.addObserver(observer);
  void subscribePattern(Observer observer) =>
      _petPattern.observable.addObserver(observer);

  void addPoints(int addedValue) => _points.add(addedValue);
  void addHunger(double addedValue) => _hunger.add(addedValue);
  void addHappiness(double addedValue) => _happiness.add(addedValue);

  void changePet(String newPet) => _pet.change(newPet);
  void changePattern(String newPattern) => _petPattern.change(newPattern);
  void changeColor(Color newColor) => _petColor.change(newColor);
}

class GameStateObservable extends Observable {}

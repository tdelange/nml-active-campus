import 'dart:ui';

import 'package:observer/observer.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'state.dart';

// T: value type, which can either be int or double
abstract class GameStateField<T> {
  final String _key;
  final Observable _observable;
  late T _value;
  GameStateField(this._key) : _observable = GameStateObservable() {
    this._value = GameState.startingValues[_key] as T;
    _initValue();
  }

  get value => _value;
  get _startingValue => GameState.startingValues[_key];
  get observable => _observable;

  Future<void> _initValue();
  Future<void> _save(String key, T value);
}

// ==================================== //
// Statusses                            //
// ==================================== //
abstract class GameStateFieldNumeric<T> extends GameStateField<T> {
  GameStateFieldNumeric(super.key);

  void add(T addedValue) {
    if (value == null) throw Exception("Help");

    if (value + addedValue <= 0) {
      _value = 0 as T;
    } else if (value + addedValue >= 1 && _key != 'points') {
      _value = 1 as T;
    } else {
      _value = value + addedValue;
    }

    _observable.notifyObservers(value);
    _save(_key, _value);
  }
}

class GameStateFieldInt extends GameStateFieldNumeric<int> {
  GameStateFieldInt(key) : super(key);

  @override
  Future<void> _initValue() async {
    final prefs = await SharedPreferences.getInstance();
    final savedValue = prefs.getInt(_key);
    _value = savedValue ?? _startingValue;
    super._observable.notifyObservers(value);
  }

  @override
  Future<void> _save(String key, int value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setInt(key, value);
  }
}

class GameStateFieldDouble extends GameStateFieldNumeric<double> {
  GameStateFieldDouble(key) : super(key);

  @override
  Future<void> _initValue() async {
    final prefs = await SharedPreferences.getInstance();
    final savedValue = prefs.getDouble(super._key);
    _value = savedValue ?? _startingValue;
    super._observable.notifyObservers(value);
  }

  @override
  Future<void> _save(String key, double value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setDouble(key, value);
  }
}

// ==================================== //
// Cosmetics                            //
// ==================================== //
abstract class CosmeticField<T> extends GameStateField<T> {
  CosmeticField(super.key);

  void change(T newValue) {
    super._value = newValue;
    _save(_key, _value);
  }
}

class CosmeticFieldString extends CosmeticField<String> {
  CosmeticFieldString(super.key);

  @override
  Future<void> _initValue() async {
    final prefs = await SharedPreferences.getInstance();
    final savedValue = prefs.getString(_key);
    _value = savedValue ?? _startingValue;
    super._observable.notifyObservers(value);
  }

  @override
  Future<void> _save(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }
}

class CosmeticFieldColor extends CosmeticField<Color> {
  CosmeticFieldColor(key) : super(key);

  @override
  Future<void> _initValue() async {
    final prefs = await SharedPreferences.getInstance();
    var savedValue = prefs.getString(_key);
    savedValue = savedValue ?? _toHex(_startingValue);
    _value = _toColor(savedValue);

    super._observable.notifyObservers(value);
  }

  @override
  Future<void> _save(String key, Color value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, _toHex(_value));
  }

  String _toHex(Color color) => '#${color.value.toRadixString(16)}';

  Color _toColor(String hex) =>
      Color(int.parse(hex.substring(1, 7), radix: 16) + 0xFF000000);
}

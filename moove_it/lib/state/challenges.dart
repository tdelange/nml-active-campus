import 'package:moove_it/state/gamestate/state.dart';

import '../models/challenge.dart';
import '../providers/challenge_provider.dart';

class Challenges {
  static final Challenges _instance = Challenges._privateConstructor();

  final GameState _gameState = GameState();
  List<Challenge> _challenges = List.empty();

  Challenges._privateConstructor() {
    getChallenges(_gameState.userId)
        .then((challenges) => _challenges = challenges);
  }

  factory Challenges() {
    return _instance;
  }

  List<Challenge> get challenges => _challenges;

  List<Challenge> get stepChallenges =>
      _challenges.where((challenge) => challenge.type == 'Steps').toList();

  int get nCompleted =>
      _challenges.where((challenge) => challenge.completed).length;
}

// Refactored from smallstepman @ https://github.com/cph-cachet/flutter-plugins/issues/56

import 'package:moove_it/providers/history_provider.dart';
import 'package:moove_it/state/gamestate/state.dart';
import 'package:observer/observer.dart';
import 'package:pedometer/pedometer.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:synchronized/synchronized.dart';

import '../providers/user_provider.dart';
import 'challenges.dart';

class CustomPedometer {
  static final CustomPedometer _instance =
      CustomPedometer._privateConstructor();

  final GameState _gameState = GameState();
  final Challenges _challenges = Challenges();

  final GameStateObservable _stepObservable = GameStateObservable();
  late final Stream<StepCount> _stepCountStream;

  int _todaySteps = 0;

  CustomPedometer._privateConstructor() {
    _initialise();
  }

  factory CustomPedometer() {
    return _instance;
  }

  int get todaySteps => _todaySteps;

  void subscribeSteps(Observer observer) =>
      _stepObservable.addObserver(observer);

  Future<void> _initialise() async {
    final status = await Permission.activityRecognition.status;

    final now = DateTime.now().millisecondsSinceEpoch;
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt("lastTimeStarved", now);

    if (status.isGranted) {
      _stepCountStream = Pedometer.stepCountStream;
      _stepCountStream.listen(_onStepCount).onError(_onStepCountError);
    } else if (await Permission.speech.isPermanentlyDenied) {
      openAppSettings();
    }

    _initTodaySteps();
  }

  Future<int> _initTodaySteps() async {
    final prefs = await SharedPreferences.getInstance();
    final todaySteps = prefs.getInt('todaySteps') ?? 0;
    _todaySteps = todaySteps;
    _stepObservable.notifyObservers(todaySteps);
    return todaySteps;
  }

  void _onStepCount(StepCount event) async {
    final lock = Lock();
    final steps = event.steps;

    final prefs = await SharedPreferences.getInstance();
    final onCampus = prefs.getBool('geofence') ?? false;

    if (onCampus) {
      final prefs = await SharedPreferences.getInstance();
      // Lock functions so they aren't being used concurrently
      lock.synchronized(() async {
        await _handleFirstRun(prefs, steps);
        _updateTodaysSteps(prefs, steps);
        _addProgress(prefs, steps);
        _spy(prefs, steps);
        _starvePet(prefs);
      });
    }
  }

  void _onStepCountError(error) {
    _stepObservable.notifyObservers(0);
  }

  // Pedometer looks at steps since last reboot, even if those happened before
  // the app was installed. This function inits those steps as handled.
  Future<void> _handleFirstRun(SharedPreferences prefs, int steps) async {
    final firstRun = prefs.getBool('firstRun') ?? true;
    if (firstRun) {
      prefs.setInt('lastDaySaved', DateTime.now().day);
      prefs.setInt('savedSteps', steps);
      prefs.setInt('processedSteps', steps);
      prefs.setInt('spiedSteps', steps);
      prefs.setBool('firstRun', false);
    }
  }

  Future<void> _updateTodaysSteps(SharedPreferences prefs, int steps) async {
    var lastDaySaved = prefs.getInt('lastDaySaved')!;
    var savedSteps = prefs.getInt('savedSteps')!;

    var todayDayNo = DateTime.now().day;
    int todaySteps;

    // Handle device reboot
    if (steps < savedSteps) {
      savedSteps = 0;
      prefs.setInt('savedSteps', savedSteps);
    }

    // Reset on day change
    if (todayDayNo > lastDaySaved) {
      _resetStepCounter(prefs, steps, todayDayNo);
    }

    // Calculate todays steps
    todaySteps = steps - savedSteps;
    _todaySteps = todaySteps;
    prefs.setInt('todaySteps', todaySteps);

    _stepObservable.notifyObservers(todaySteps);
  }

  void _resetStepCounter(SharedPreferences prefs, int steps, int todayDayNo) {
    var todaySteps = prefs.getInt('todaySteps') ?? 0;
    _sendHistory(_gameState, _challenges, todaySteps);

    todaySteps = 0;
    prefs.setInt('lastDaySaved', todayDayNo);
    prefs.setInt('savedSteps', steps);
    prefs.setInt('todaySteps', 0);
  }

  // Processes points and challenge progress
  Future<void> _addProgress(SharedPreferences prefs, int steps) async {
    const int stepsPerPoint = 10;

    var processedSteps = prefs.getInt('processedSteps')!;

    // Handle device reboot
    if (steps < processedSteps) {
      processedSteps = 0;
      prefs.setInt('processedSteps', processedSteps);
    }

    // Calculate new points
    var newSteps = steps - processedSteps;
    _gameState.addPoints(newSteps ~/ stepsPerPoint);
    _addChallengeProgress(todaySteps);

    //  Update happiness
    var happiness = _gameState.hunger >= .75 ? 0.0004 : 0.0003;
    _gameState.addHappiness(newSteps * happiness);

    // Save processed steps
    var leftoverSteps = newSteps % stepsPerPoint;
    processedSteps += newSteps - leftoverSteps;
    prefs.setInt('processedSteps', processedSteps);
  }

  void _addChallengeProgress(int newSteps) {
    for (final challenge in _challenges.stepChallenges) {
      challenge.keepProgress(_gameState, newSteps);
    }
  }

  void _starvePet(SharedPreferences prefs) {
    const starvationFrequencyMili = 15 * 60 * 1000;
    final lastTimeStarved = prefs.getInt("lastTimeStarved") ??
        DateTime.now().millisecondsSinceEpoch;
    final now = DateTime.now().millisecondsSinceEpoch;
    final int starvationTimes =
        ((now - lastTimeStarved) / starvationFrequencyMili).floor();

    if (starvationTimes <= 0) return;

    for (var i = 0; i < starvationTimes; i++) {
      const starvationAmount = -0.06725;
      final upsetAmount = _gameState.hunger <= .25 ? -0.06725 : -0.06525;

      _gameState.addHunger(starvationAmount);
      _gameState.addHappiness(upsetAmount);
    }

    prefs.setInt("lastTimeStarved", now);
  }

  void _spy(SharedPreferences prefs, int steps) {
    const spyFrequency = 100;
    if (todaySteps <= 0) return;

    var spiedSteps = prefs.getInt("spiedSteps")!;

    // Handle device reboot
    if (steps < spiedSteps) {
      spiedSteps = 0;
      prefs.setInt('spiedSteps', spiedSteps);
    }

    final stepsToSpy = steps - spiedSteps;
    if (stepsToSpy < spyFrequency) return;

    prefs.setInt("spiedSteps", steps);
    updateUserSteps(todaySteps, _gameState.userId);
  }

  void _sendHistory(
      GameState gameState, Challenges challenges, int dailySteps) {
    postHistory(
        gameState.userId, gameState.points, dailySteps, challenges.nCompleted);
  }
}

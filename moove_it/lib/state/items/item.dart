import 'package:flutter/material.dart';
import 'package:observer/observer.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../gamestate/state.dart';

abstract class Item {
  final String name;
  final int price;
  final GameState gameState;

  final Observable _observable;

  int _nOwned = 0;

  Item(this.name, this.price, this.gameState, this._observable) {
    _init();
  }

  get nOwned => _nOwned;

  void buy(BuildContext context) {
    if (gameState.points < price) {
      const confirmationMessage = SnackBar(
          content: Text('You do not have enough points'),
          duration: Duration(milliseconds: 500));
      ScaffoldMessenger.of(context).showSnackBar(confirmationMessage);
      return;
    }

    gameState.addPoints(-price);
    _nOwned++;
    _observable.notifyObservers(true);
    _save(name, _nOwned);

    const confirmationMessage = SnackBar(
        content: Text('Item purchased!'),
        duration: Duration(milliseconds: 200));
    ScaffoldMessenger.of(context).showSnackBar(confirmationMessage);
  }

  void consume() {
    _nOwned--;
    _observable.notifyObservers(true);
    _save(name, _nOwned);
  }

  void use(BuildContext context);

  Future<void> _init() async {
    // Free items (cosmetics) are always owned
    if (price <= 0) {
      _nOwned = 1;
      return;
    }

    final prefs = await SharedPreferences.getInstance();
    final savedNOwned = prefs.getInt(name);
    _nOwned = savedNOwned ?? 0;
  }

  Future<void> _save(String name, int nOwned) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setInt(name, nOwned);
  }
}

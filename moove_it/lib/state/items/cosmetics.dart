import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'item.dart';

abstract class Cosmetic extends Item {
  Cosmetic(name, price, gameState, observable)
      : super(name, price, gameState, observable);

  @override
  void buy(BuildContext context) {
    if (nOwned > 1) return;
    super.buy(context);
  }

  void confirmUsed(BuildContext context, String type) {
    final confirmationMessage = SnackBar(
        content: Text('$type applied!'),
        duration: const Duration(milliseconds: 200));
    ScaffoldMessenger.of(context).showSnackBar(confirmationMessage);
  }
}

class CosmeticColor extends Cosmetic {
  final Color color;

  CosmeticColor(name, price, this.color, gameState, observable)
      : super(name, price, gameState, observable);

  @override
  void use(BuildContext context) async {
    gameState.changeColor(color);
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('petColor', color.toString());

    super.confirmUsed(context, 'Color');
  }
}

class CosmeticPattern extends Cosmetic {
  final String petPattern;

  CosmeticPattern(name, price, this.petPattern, gameState, observable)
      : super(name, price, gameState, observable);

  @override
  void use(BuildContext context) async {
    gameState.changePattern(petPattern);
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('petPattern', petPattern);

    super.confirmUsed(context, 'Pattern');
  }
}

class CosmeticType extends Cosmetic {
  final String petType;

  CosmeticType(name, price, this.petType, gameState, observable)
      : super(name, price, gameState, observable);

  @override
  void use(BuildContext context) async {
    gameState.changePet(petType);
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('pet', petType);

    super.confirmUsed(context, 'Pet');
  }
}

import 'package:flutter/material.dart';

import 'item.dart';

class Food extends Item {
  final double nutritionalValue;
  Food(this.nutritionalValue, name, price, gameState, observable)
      : super(name, price, gameState, observable);

  @override
  void use(BuildContext context) {
    if (nOwned <= 0) return;
    consume();
    gameState.addHunger(nutritionalValue);

    const confirmationMessage = SnackBar(
        content: Text('Nom nom nom!'), duration: Duration(milliseconds: 200));
    ScaffoldMessenger.of(context).showSnackBar(confirmationMessage);
  }
}

import 'package:flutter/material.dart';
import 'package:observer/observer.dart';

import '../gamestate/state.dart';
import 'consumables.dart';
import 'cosmetics.dart';
import 'item.dart';

class Inventory {
  static final Inventory _instance = Inventory._privateConstructor();
  final GameState gameState = GameState();
  final _observable = GameStateObservable();

  late final List<Item> _foods;
  late final List<Item> _patterns;
  late final List<Item> _pets;
  late final List<Item> _colors;

  Inventory._privateConstructor() {
    _foods = [
      Food(0.5, 'Apple', 50, gameState, _observable),
      Food(0.1, 'Cookie', 15, gameState, _observable),
      Food(0.2, 'Burger', 25, gameState, _observable),
      Food(0.3, 'Banana', 30, gameState, _observable),
      Food(0.4, 'Bread', 45, gameState, _observable),
    ];

    _patterns = [
      CosmeticPattern('None', 0, 'no-pattern', gameState, _observable),
      CosmeticPattern('Checkers', 100, 'checkers', gameState, _observable),
      CosmeticPattern('Mosaic', 100, 'mosaic', gameState, _observable),
      CosmeticPattern('Lava', 100, 'lava', gameState, _observable),
      CosmeticPattern('Cubes', 100, 'cubes', gameState, _observable),
    ];

    _colors = [
      CosmeticColor('White', 0, const Color.fromRGBO(255, 255, 255, 0.0),
          gameState, _observable),
      CosmeticColor('Blue', 80, const Color.fromRGBO(33, 150, 243, 0.4),
          gameState, _observable),
      CosmeticColor('Red', 80, const Color.fromRGBO(244, 67, 54, 0.4),
          gameState, _observable),
      CosmeticColor('Green', 80, const Color.fromRGBO(76, 175, 80, 0.4),
          gameState, _observable),
      CosmeticColor('Yellow', 80, const Color.fromRGBO(255, 235, 59, 0.4),
          gameState, _observable),
    ];
    _pets = [
      CosmeticType('Cow', 0, 'cow', gameState, _observable),
      CosmeticType('Cat', 600, 'cat', gameState, _observable),
      CosmeticType('Dog', 600, 'dog', gameState, _observable),
      CosmeticType('Turtle', 600, 'turtle', gameState, _observable),
      CosmeticType('Lizard', 600, 'lizard', gameState, _observable),
    ];
  }

  factory Inventory() {
    return _instance;
  }

  get foods => _foods;
  get patterns => _patterns;
  get pets => _pets;
  get colors => _colors;

  void subscribe(Observer observer) => _observable.addObserver(observer);

  Map<String, List<Item>> get items =>
      {'Foods': foods, 'Patterns': patterns, 'Pets': pets, 'Colors': colors};

  // Returns consumables and owned cosmetics
  Map<String, List<Item>> get ownedItems {
    return items.map((category, itemsList) {
      if (category == 'Foods') return MapEntry(category, itemsList);

      final filteredItems = itemsList.where((item) => item.nOwned > 0).toList();
      return MapEntry(category, filteredItems);
    });
  }

  // Returns consumables and unowned cosmetics
  Map<String, List<Item>> get unownedItems {
    return items.map((category, itemsList) {
      if (category == 'Foods') return MapEntry(category, itemsList);

      final filteredItems =
          itemsList.where((item) => item.nOwned <= 0).toList();
      return MapEntry(category, filteredItems);
    });
  }
}

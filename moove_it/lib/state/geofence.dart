import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geofence_service/geofence_service.dart';
import 'package:moove_it/state/gamestate/state.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CustomGeofence {
  final GameStateObservable _geofenceObservable = GameStateObservable();
  late final StreamController<Geofence> _geofenceStreamController;
  late final GeofenceService _geofenceService = GeofenceService.instance.setup(
      interval: 30000,
      accuracy: 100,
      loiteringDelayMs: 60000,
      statusChangeDelayMs: 10000,
      useActivityRecognition: true,
      allowMockLocations: false,
      printDevLog: false,
      geofenceRadiusSortType: GeofenceRadiusSortType.DESC);
  late final List<Geofence> _geofenceList = <Geofence>[
    Geofence(
      id: 'campus',
      latitude: 51.821692,
      longitude: 5.863638,
      radius: [
        GeofenceRadius(id: 'radius_600m', length: 600),
      ],
    ),
  ];

  CustomGeofence() {
    _initialize();
  }

  Future<void> _initialize() async {
    final status = await Permission.locationAlways.status;

    final prefs = await SharedPreferences.getInstance();
    final geofence = prefs.setBool('geofence', false);

    if (status.isGranted) {
      _geofenceStreamController = StreamController();
      _initGeofence();

      WidgetsBinding.instance.addPostFrameCallback((_) {
        _geofenceService
            .addGeofenceStatusChangeListener(_onGeofenceStatusChanged);
        _geofenceService.addLocationChangeListener(_onLocationChanged);
        _geofenceService.addLocationServicesStatusChangeListener(
            _onLocationServicesStatusChanged);
        _geofenceService.addStreamErrorListener(_onError);
        _geofenceService.start(_geofenceList).catchError(_onError);
      });
    } else if (await Permission.speech.isPermanentlyDenied) {
      openAppSettings();
    }
  }

  Future<bool> _initGeofence() async {
    final prefs = await SharedPreferences.getInstance();
    final geofence = prefs.getBool('geofence') ?? false;
    _geofenceObservable.notifyObservers(geofence);
    return geofence;
  }

// This function is to be called when the geofence status is changed.
  Future<void> _onGeofenceStatusChanged(
      Geofence geofence,
      GeofenceRadius geofenceRadius,
      GeofenceStatus geofenceStatus,
      Location location) async {
    print('geofence: ${geofence.toJson()}');
    print('geofenceRadius: ${geofenceRadius.toJson()}');
    print('geofenceStatus: ${geofenceStatus.toString()}');
    _geofenceStreamController.add(geofence);

    var prefs = await SharedPreferences.getInstance();
    if (geofence.id == 'campus') {
      prefs.setBool('geofence', true);
    } else {
      prefs.setBool('geofence', false);
    }
  }

// This function is to be called when the location has changed.
  void _onLocationChanged(Location location) {
    print('location: ${location.toJson()}');
  }

// This function is to be called when a location services status change occurs
// since the service was started.
  void _onLocationServicesStatusChanged(bool status) {
    print('isLocationServicesEnabled: $status');
  }

// This function is used to handle errors that occur in the service.
  void _onError(error) {
    final errorCode = getErrorCodesFromError(error);
    if (errorCode == null) {
      print('Undefined error: $error');
      return;
    }

    print('ErrorCode: $errorCode');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // A widget used when you want to start a foreground task when trying to minimize or close the app.
      // Declare on top of the [Scaffold] widget.
      home: WillStartForegroundTask(
        onWillStart: () async {
          // You can add a foreground task start condition.
          return _geofenceService.isRunningService;
        },
        androidNotificationOptions: AndroidNotificationOptions(
          channelId: 'geofence_service_notification_channel',
          channelName: 'Geofence Service Notification',
          channelDescription:
              'This notification appears when the geofence service is running in the background.',
          channelImportance: NotificationChannelImportance.LOW,
          priority: NotificationPriority.LOW,
          isSticky: false,
        ),
        iosNotificationOptions: const IOSNotificationOptions(),
        foregroundTaskOptions: const ForegroundTaskOptions(),
        notificationTitle: 'Geofence Service is running',
        notificationText: 'Tap to return to the app',
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Geofence Service'),
            centerTitle: true,
          ),
          body: _buildContentView(),
        ),
      ),
    );
  }

  Widget _buildContentView() {
    return ListView(
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.all(8.0),
      children: [
        const SizedBox(height: 20.0),
        _buildGeofenceMonitor(),
      ],
    );
  }

  Widget _buildGeofenceMonitor() {
    return StreamBuilder<Geofence>(
      stream: _geofenceStreamController.stream,
      builder: (context, snapshot) {
        final updatedDateTime = DateTime.now();
        final content = snapshot.data?.toJson().toString() ?? '';

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('•\t\tGeofence (updated: $updatedDateTime)'),
            const SizedBox(height: 10.0),
            Text(content),
          ],
        );
      },
    );
  }
}

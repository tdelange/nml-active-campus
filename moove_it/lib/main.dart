import 'package:flutter/material.dart';
import 'package:moove_it/home_page/home_page.dart';
import 'package:moove_it/shop_page.dart';
import 'package:moove_it/challenge_page.dart';
import 'package:moove_it/leaderboard_page.dart';
import 'package:moove_it/inventory_page.dart';
import 'package:moove_it/state/challenges.dart';
import 'package:moove_it/state/geofence.dart';
import 'package:moove_it/state/items/inventory.dart';
import 'package:moove_it/state/pedometer.dart';
import 'package:moove_it/state/gamestate/state.dart';
import 'package:permission_handler/permission_handler.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        scaffoldBackgroundColor: Color.fromARGB(255, 248, 240, 227),
        primarySwatch: Colors.brown,
      ),
      home: const MainPage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MainPage extends StatefulWidget {
  const MainPage({super.key, required this.title});
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MainPage> createState() => _MainPage();
}

class _MainPage extends State<MainPage> {
  late final GameState gameState;
  late final Challenges challenges;
  late final CustomPedometer pedometer;
  late final CustomGeofence geofence;
  late final Inventory inventory;

  late final List<Widget> screens;

  int currentPageIndex = 2;

  _MainPage() {
    requestPermissions();

    gameState = GameState();
    challenges = Challenges();
    geofence = CustomGeofence();
    pedometer = CustomPedometer();
    inventory = Inventory();

    screens = [
      ShopPage(inventory),
      InventoryPage(inventory),
      HomePage(gameState, pedometer),
      ChallengePage(gameState),
      const LeaderboardPage(),
    ];
  }

  Future<void> requestPermissions() async {
    // Request the permissions
    await Permission.activityRecognition.request();
    await Permission.location.request();
    await Permission.notification.request();
    await Permission.locationAlways.request();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        bottomNavigationBar: NavigationBar(
          onDestinationSelected: (int index) {
            setState(() {
              currentPageIndex = index;
            });
          },
          selectedIndex: currentPageIndex,
          destinations: const <Widget>[
            NavigationDestination(
              selectedIcon: Icon(Icons.shopping_cart),
              icon: Icon(Icons.shopping_cart_outlined),
              label: '',
            ),
            NavigationDestination(
              selectedIcon: Icon(Icons.inventory),
              icon: Icon(Icons.inventory_2_outlined),
              label: '',
            ),
            NavigationDestination(
              selectedIcon: Icon(Icons.home),
              icon: Icon(Icons.home_outlined),
              label: '',
            ),
            NavigationDestination(
              selectedIcon: Icon(Icons.star),
              icon: Icon(Icons.star_border_outlined),
              label: '',
            ),
            NavigationDestination(
              selectedIcon: Icon(Icons.leaderboard),
              icon: Icon(Icons.leaderboard_outlined),
              label: '',
            ),
          ],
        ),
        body: SafeArea(
            child: screens[
                currentPageIndex])); // SafeArea is to avoid statusbar collision with our widgets
  }
}

import 'package:flutter/material.dart';
import 'package:moove_it/models/challenge.dart';
import 'package:moove_it/providers/challenge_provider.dart';
import 'package:moove_it/state/gamestate/state.dart';

class ChallengePage extends StatefulWidget {
  final GameState _gameState;
  const ChallengePage(this._gameState, {super.key});
  @override
  State<ChallengePage> createState() => _ChallengePageState(_gameState);
}

class _ChallengePageState extends State<ChallengePage> {
  final GameState _gameState;
  late Future<List<Challenge>> challenges;

  _ChallengePageState(this._gameState) {
    super.initState();
    challenges = getChallenges(_gameState.userId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Challenges'),
      ),
      body: FutureBuilder<List<Challenge>>(
          future: challenges,
          builder: (context, snapshot) {
            if (snapshot.data == null) {
              return const Text("Loading");
            } else {
              var data = (snapshot.data as List<Challenge>).toList();
              return ListView.separated(
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    return ChallengeCard(challenge: data[index]);
                  });
            }
          }),
    );
  }
}

class ChallengeCard extends StatelessWidget {
  static const double iconSize = 50.0;
  final Challenge challenge;
  const ChallengeCard({super.key, required this.challenge});

  @override
  Widget build(BuildContext context) {
    return Card(
        color: challenge.completed ? Color.fromRGBO(60, 200, 60, 0.3) : null,
        child: ListTile(
          title: Text(challenge.challengeDescription),
          subtitle: Text('Reward: ${challenge.points} points'),
          onTap: () => {
            //TODO: start challenge?
          },
          leading: const Icon(
            Icons.attach_money,
            color: Colors.green,
            size: iconSize,
          ),
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:moove_it/items_view/item_button.dart';
import 'package:moove_it/state/gamestate/state.dart';
import 'package:moove_it/state/items/inventory.dart';
import 'package:observer/observer.dart';

import 'items_view/category_header.dart';
import 'state/items/item.dart';

class InventoryPage extends StatefulWidget {
  static const int nColumns = 3;
  final Inventory inventory;

  InventoryPage(this.inventory, {super.key});

  @override
  State<StatefulWidget> createState() =>
      _InventoryPageState(inventory: inventory);
}

class _InventoryPageState extends State<InventoryPage> with Observer {
  Inventory inventory;
  _InventoryPageState({required this.inventory}) {
    inventory.subscribe(this);
  }

  @override
  void update(Observable observable, Object arg) {
    if (mounted) {
      setState(() {
        // Re-renders widget if mounted (open)
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Inventory'),
        ),
        body: ListView(
            children: inventory.ownedItems.entries
                .map<InventoryCategoryGrid>((entry) => InventoryCategoryGrid(
                    nColumns: InventoryPage.nColumns,
                    category: entry.key,
                    items: entry.value))
                .toList()));
  }
}

class InventoryCategoryGrid extends StatelessWidget {
  static const double rowMargin = 1.5;
  final int nColumns;
  final String category;
  final List<Item> items;

  const InventoryCategoryGrid(
      {super.key,
      required this.nColumns,
      required this.category,
      required this.items});

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      CategoryHeader(category: category),
      GridView.count(
          physics: NeverScrollableScrollPhysics(),
          crossAxisCount: nColumns,
          shrinkWrap: true,
          childAspectRatio: rowMargin,
          children: [
            for (final item in items)
              if (category == 'Foods')
                ItemButton(
                    itemName: item.name,
                    subText: 'Owned: ${item.nOwned}',
                    onPressed: () => item.use(context))
              else
                ItemButton(
                    itemName: item.name,
                    subText: 'Owned',
                    onPressed: () => item.use(context))
          ])
    ]);
  }
}
